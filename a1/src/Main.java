import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> primeNumber = new ArrayList<>(Arrays.asList(2, 3, 5, 7, 11));
        /*primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;*/
        System.out.println("The first prime number is: "+ primeNumber.get(0));
//        System.out.println("The second prime number is: "+ primeNumber.get(1));
//        System.out.println("The third prime number is: "+ primeNumber.get(2));
//        System.out.println("The fourth prime number is: "+ primeNumber.get(3));
//        System.out.println("The fifth prime number is: "+ primeNumber.get(4));

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: "+ friends);

        HashMap<String, Integer> stocks = new HashMap<>();
        stocks.put("toothpaste", 15);
        stocks.put("toothbrush", 20);
        stocks.put("soap", 12);
        System.out.println("Our current inventory consists of: "+ stocks);
    }


}