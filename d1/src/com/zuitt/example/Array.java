package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        // [SECTION] Java Collection
        /*
        * are single unit of objects
        * data with relevant and connected values
        * */

        // [SECTION] Array
        /*
        * In java, arrays are container of values of the same type given a predefined amount of values.
        * java arrays are more rigid, once the size and data type are defined, they can no longer be changed.
        *
        * Syntax:
        * dataType[] identifier = new dataType[numOfElements];
        **/

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        // will result error -> out of bounds
        // intArray[5] = 100;

        // this will return memory address
        System.out.println(intArray);
        System.out.println(Arrays.toString(intArray));

        // String Array
        /*
        * Syntax: Array Declaration initialization
        * dataType[] identifier = {elementA, elementB, elementC};
        * */

        String[] names = {"John","Jane","Joe"};
        System.out.println(Arrays.toString(names));

        // Sample Java Array Method

        // Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort() method "+ Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        // First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Joebert";

        // Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        // [SECTION] ArrayList
        /*
        * are resizable arrays wherein elements can be added or removed
        *
        * Syntax: ArrayList
        * ArrayList<T> identifier = new ArrayList<T>;
        * */

        // Declaring ArrayList
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        // Add Elements
        students.add("John");
        students.add("Paul");
        students.add("Anne");

        System.out.println(students);

        // Getting an element
        System.out.println(students.get(0));

        // Adding an element in a specific index
        students.add(1, "Joey");
        System.out.println(students);

        // Update element in a specific index
        students.set(0, "George");
        System.out.println(students);

        // Remove element in a specific index
        students.remove(1);
        System.out.println(students);
        System.out.println(students.size());

        // Remove all element
        students.clear();
        System.out.println(students);

        // Get ArrayList size
        System.out.println(students.size());

        // [SECTION] HashMaps
        /*
        * Syntax: HashMaps
        * HashMap<T, T> identifier = new HashMap<T, T>();
        * */

        HashMap<String, Integer> job_position = new HashMap<String, Integer>();

        // Adding element to a HashMap
        job_position.put("Brandon", 5);
        job_position.put("Alice", 3);

        System.out.println(job_position);
        System.out.println(job_position.get("Brandon"));
        System.out.println(job_position.values());
        job_position.clear();
        System.out.println(job_position);
    }
}
