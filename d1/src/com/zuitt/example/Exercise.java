package com.zuitt.example;

import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {
        Scanner inputYear = new Scanner(System.in);

        System.out.println("Enter year:");
        int year = inputYear.nextInt();

        if(year % 4 == 0){
            if(year % 100 == 0){
                if (year % 400 == 0){
                    System.out.println(year + " is a leap year");
                } else{
                    System.out.println(year + " is not a leap year");
                }
            } else{
                System.out.println(year + " is a leap year");
            }
        } else{
            System.out.println(year + " is not a leap year");
        }
    }
}
