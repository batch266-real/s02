package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        // [SECTION] Java Operators
        /*
        * Arithmetic -> +, - , *, /, %
        * Comparison -> >, <, >=, <=, ==, !=
        * Logical -> &&, ||, !
        * Assignment -> =
        * */

        // [SECTION] Control Structure (if else statement)

        int num = 20;

        if(num % 5 == 0){
            System.out.println(num +" is divisible by 5");
        } else{
            System.out.println(num +" is not divisible by 5");
        }

        // Ternary Operator
        int grade = 60;

        Boolean result = (grade >= 75) ? true : false;
        System.out.println(result);

        // [SECTION] Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number:");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            // A case block within a switch statement. Represents a single case.
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("East");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("South");
                break;
            default:
                System.out.println("Invalid input");
                break;
        }

    }
}
